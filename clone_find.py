from tokenizer.tokenizer import (
                                  fingerprint,
                                  getFingerprints
                                )
from database.dbAdapter import connect
from git_proc.git_proc import access_gitDB
from git import Blob
from sim_detect.sim_detect import sortOutQueryResult, simCal, find_duplicates
from report_gen.report_gen import genTable, createHTMLReport
from util import get_staged_files
from objects.similar import SimilarRating
from decimal import *
from argparse import ArgumentParser
import os
import subprocess
import sys

getcontext().prec = 3

if __name__ == "__main__":
  parser = ArgumentParser(prog="CLone Finder", description="Find similarity of staged files and comitted files of the git repository")
  parser.add_argument('-r', action="store",dest="repository",help='Path of Git repository', required=False, type=str)
  parser.add_argument('-host', action="store", dest="host", help='Host of MySQL database', required=True, type=str)
  parser.add_argument('-u', action="store", dest="user", help='MySQL user', required=True, type=str)
  parser.add_argument('-p', action="store", dest="pwd", help="MySQL pass", required=True, type=str)
  parser.add_argument('-d', action="store", dest="database", help="MySQL database", required=True, type=str)
  parser.add_argument('-t', action="store", dest="db_table", help="MySQL table",required=False, type=str)
  parser.add_argument('-s', action="store", dest="similarity", help="If not specified, full report will be given", required=False, type=Decimal)
  parser.add_argument('-f', action="store", dest="filename", help="Summary report name", required=False, type=str)
  #parser.add_argument("-a","--auto",action="store",dest="auto_start", help="Open summary report when process is done.", required=False, type=bool)
  parser.add_argument('--version', action="version",version="%(prog)s 0.9.1")

  arguments = parser.parse_args()

  # Connect to MySQL database
  database = connect(arguments.database)
  db = database[0]
  cursor = database[1]

  # Access Git repository
  repo = access_gitDB(arguments.repository)

  files = get_staged_files(cursor,arguments.db_table,repo)
  # Terminate if no stage file can be found
  if files is None:
    print Exception("No staged file.")
    print "Ending process"
    sys.exit(0)

  print "Similarity search..."
  result = []
  sim_result = []
  similar_rating_objects = []
  for f in files:
    # Get data stream from repository
    blob = Blob(repo,f.binsha)
    content = blob.data_stream.read()
    fp_set = fingerprint(content)
    fp_tuple = getFingerprints(fp_set)

    '''
    Object similar rating:
    It stores object blob and it's fingerprint, including the similarity of each individual result
    '''
    similar_rating = SimilarRating(f,fp_tuple)
    find_duplicates(cursor,arguments.db_table,similar_rating)
    similar_rating_objects.append(similar_rating)
  print "Search done."
  for similar_rating_obj in similar_rating_objects:
    similar_rating_obj.calculate_similarity()



  # Generate report
  table = genTable(repo,similar_rating_objects)

  # Create report
  createHTMLReport(table,filename=arguments.filename)

  '''
  if arguments.auto_start is not None:
    if bool(arguments.auto_start) is True:
      try:
        retcode = subprocess.call("open "+arguments.filename,shell=True)
        if retcode < 0:
          print >>sys.stderr, "Child was terminate by signal", -retcode
        else:
          print >>sys.stderr, "Child returned", retcode
      except OSError, e:
        print >>sys.stderr, "Execution failed:", e
  '''
