from decimal import *

# Decimal number precision
getcontext().prec = 4

class SimilarRating:
  def __init__(self,blob,fingerprints):
    self.blob = blob
    self.fp = fingerprints
    self.hits = []
    # Contain details of each blob id and commit id
    self.calculated_hits = dict()
    # only contain similarity of associated blob and commit id
    self.similar = dict()

  def getBlob(self):
    return self.blob

  def getFingerprints(self):
    return self.fp

  def addHits(self,hit):
    self.hits.append(hit)

  def getHits(self):
    return self.hits

  def calculate_similarity(self):
    # Go through every single hits construct dictionary
    for hit in self.hits:
      for h in hit:
        commit_id = h[0]
        blob_path = h[1]
        blob_id = h[2]
        blob_hash = h[3]
        keys = self.calculated_hits.keys()
        # Get the information above into the dict data structure
        if (commit_id,blob_id) in keys:
            self.calculated_hits[(commit_id,blob_id)].append((blob_path,blob_hash))
        else:
          self.calculated_hits[(commit_id,blob_id)] = [(blob_path,blob_hash)]
    keys = self.calculated_hits.keys()
    for key in keys:
      similar = Decimal(len(self.calculated_hits[key]))/Decimal(len(self.fp))*100
      self.similar[key] = [similar]

  def get_similarity_summary(self):
    return self.calculated_hits

  def simValue(self):
    return self.similar
