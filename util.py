from database import dbAdapter
from git_proc import git_proc
from git import Blob

# Get staged files
def get_staged_files(cursor,table,repo):
  # Get staged files
  index = repo.index
  stage_files = []
  # Find file which is newly index
  for entry in index.entries.itervalues():
    # Query in database
    query = dbAdapter.filterHash(cursor,table,entry.hexsha)
    if query is None:
      stage_files.append(entry)
  if len(stage_files) == 0:
    return None
  return stage_files
