from argparse import ArgumentParser
from database import dbAdapter
from tokenizer import tokenizer
from sys import dont_write_bytecode
from git_proc import git_proc
dont_write_bytecode = True

if __name__ == "__main__":
  # Command-line parameter definition of program
  parser = ArgumentParser(prog="Hash database creation tool", description="Create MySQL hash database for repository.")
  parser.add_argument('-r', action="store",dest="repository",help='Path of Git repository', required=True, type=str)
  parser.add_argument('-host', action="store", dest="host", help='Host of MySQL database', required=True, type=str)
  parser.add_argument('-u', action="store", dest="user", help='MySQL user', required=True, type=str)
  parser.add_argument('-p', action="store", dest="pwd", help="MySQL pass", required=True, type=str)
  parser.add_argument('-d', action="store", dest="database", help="MySQL database", required=True, type=str)
  parser.add_argument('-t', action="store", dest="db_table", help="MySQL table",required=True, type=str)
  parser.add_argument('--version', action="version",version="%(prog)s 0.9.1")
  arguments = parser.parse_args()

  # Connect to MySQL database
  database = dbAdapter.connect(arguments.database)
  db = database[0]
  cursor = database[1]

  # Attempt to create table
  dbAdapter.createTable(cursor,arguments.db_table)

  # Connect to git repository database
  repo = git_proc.access_gitDB(arguments.repository)

  # Read from repository
  print "Reading from repository..."
  commits = git_proc.getCommits(repo)
  trees = git_proc.getTrees(repo,commits)
  blobs = git_proc.getBlobs(trees)
  print "Done reading from repository."

  print "Fingerprinting blobs..."
  # Inserting into database
  for b in blobs:
    commit = b[0]
    blob = b[1]
    fp_set = tokenizer.fingerprint(blob.data_stream.read())
    fp_tuple = tokenizer.getFingerprints(fp_set)
    for fp in fp_tuple:
      dbAdapter.insert(db,cursor, arguments.db_table, commit, blob, fp)
  print "Done fingerprinting"

  print "Disconnecting from database"
  # Disconnect from MySQL database
  dbAdapter.disconnect(db)
