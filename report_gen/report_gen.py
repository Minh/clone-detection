from html import HTML
from git import Object, IndexObject, Blob
# Generate report HTML compatible
def genTable(repo,similar_rating_objects):
  h = HTML()
  t = h.table(border='1')
  x = t.tr
  #x.td('Commit ID')
  x.td('Blob ID of staged file')
  x.td('Staged file path')
  x.td('Commit ID')
  x.td('Blob from database')
  x.td('File')
  x.td("'%' similar")
  x = t.tr

  for similar_rating_object in similar_rating_objects:
    staged_blob = similar_rating_object.getBlob()
    keys = similar_rating_object.get_similarity_summary().keys()
    for key in keys:
      x.td(staged_blob.hexsha)
      x.td(staged_blob.path)

      commit_id = key[0]
      x.td(commit_id)
      blob_id = key[1]
      x.td(blob_id)

      values = similar_rating_object.get_similarity_summary()[key]
      blob_path = values[0][0]
      x.td(blob_path)

      similarity = similar_rating_object.simValue()[key]
      x.td(str(similarity))

      x = t.tr
  return t

# Create report based on table parameter
def createHTMLReport(table,filename="Similarity report.html"):
  with open(filename,'w') as report_file:
    report_file.write(str(table))
    report_file.close()
