import MySQLdb
from warnings import filterwarnings
from _mysql_exceptions import (
                                OperationalError,
                                ProgrammingError,
                                DataError,
                                IntegrityError
                              )

# Surpress MySQLdb warnings
filterwarnings('ignore',category=MySQLdb.Warning)

# connect to database
def connect(database,host="localhost",user="root",passwd="09V0nzfn"):
  db = None
  cursor = None
  try:
    db = MySQLdb.connect(host,user,passwd,database)
  except:
    db = MySQLdb.connect(host,user,passwd)
  finally:
    cursor = db.cursor()
    sql = """CREATE DATABASE IF NOT EXISTS {0}""".format(database)
    try:
      cursor.execute(sql)
      sql = """USE {0}""".format(database)
      cursor.execute(sql)
    except:
      raise
  return (db,cursor)

# Create pre-defined table
# ID, commit_id, blob_id, block, hash
# Create table if it does not exist
def createTable(cursor,table):
  sql = """CREATE TABLE IF NOT EXISTS {0} (commit_id varchar(225) NOT NULL, \
  blob_path varchar(255) NOT NULL ,blob_id varchar(255) NOT NULL, hash varchar(255) NOT NULL, \
  PRIMARY KEY(commit_id,blob_id,hash), UNIQUE (hash,blob_id,commit_id)) DEFAULT CHARSET=utf8""".format(table)

  try:
    cursor.execute(sql)
  except:
    raise

# Final duplicate check
def duplicate(cursor,table):
  sql = "SELECT blob_id,hash,count(*) no_of_records FROM {0} \
  group by blob_id,hash having count(*) > 1".format(table)
  try:
    cursor.execute(sql)
    query = cursor.fetchall()
    if len(query) == 0:
      print "No duplicates in", table
      return False
    else:
      print "Duplicates in", table
      print query
      return True
  except:
    raise

# insert into database
def insert(db,cursor, table, commit, blob, blob_hash):
  sql="INSERT INTO {0} (commit_id,blob_path,blob_id,hash) \
  VALUES({1},{2},{3},{4})".format(table,repr(commit),repr(blob.abspath),repr(blob.hexsha),repr(blob_hash))

  try:
    cursor.execute(sql)
    db.commit()
  except IntegrityError:
    db.rollback()
    print Exception("Integrity Error")
    print sql
    print "\n"
    pass
  except:
    db.rollback()
    raise

# query one hash from database
def selectHash(cursor,table,blob_id,hashes):
  try:
    sql = "SELECT commit_id, blob_path, blob_id, hash FROM {0} \
    WHERE hash={1} AND blob_id !={2}".format(table,hashes,repr(blob_id))

    cursor.execute(sql)
    fields = cursor.fetchall()
    return fields
  except:
    raise

# Return None if does not exist in database or return a set of value(s)
def filterHash(cursor,table,blob_id):
  try:
    sql = "SELECT blob_id FROM {0} WHERE blob_id={1}".format(table,repr(blob_id))
    cursor.execute(sql)
    return cursor.fetchone()
  except:
    raise

# Able to query multiple hashes at once
def hashQuery(cursor,table,hashes):
  try:
      args = list(repr(x) for x in hashes)
      sql = "SELECT commit_id, blob_path, blob_id, hash FROM " + table + " WHERE hash IN (%s)"
      in_p = ', '.join(map(lambda x: '%s', args))
      sql = sql % in_p
      cursor.execute(sql, args)
      fields = cursor.fetchall()
      return fields
  except:
    raise

# disconnect from database
def disconnect(db):
  try:
    db.close
  except:
    raise
