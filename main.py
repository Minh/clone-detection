import sys
import os
import inspect
path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.append(path +'/git_proc/')
sys.path.append(path +'/tokenizer/')
sys.path.append(path +'/database/')
sys.path.append(path +'/config/')
sys.path.append(path +'/sim_detect/')

sys.dont_write_bytecode = True

from git_proc import git_proc
from tokenizer import hashing
from tokenizer import tokenizer
from database import dbAdapter
from config import config_parser
from sim_detect import sim_detect

#print "Path:", path
repo = git_proc.access_gitDB(path)
if repo == None:
  print "Not a repo"
commits = git_proc.getCommits(repo)
trees = git_proc.getTrees(commits,repo)
blobs = git_proc.getBlobs(trees)

print "Connecting to database"
db_info = dbAdapter.connect('anagram_hash')
db = db_info[0]
cursor = db_info[1]
table = 'blob_hashs'
dbAdapter.createTable(cursor, table)

#print "Blob set:"
#for blob in blobs:
#  print blob
#print "# of blobs:",len(blobs)
#print "\n"
record = []
# Filter from blobs based on their ids via database query
for blob in blobs:
  if dbAdapter.filterHash(cursor,table,blob[1]) == None:
    # Delete all blob with the relate id from list
    record.append(blob)
    # hashing only necessary blob content and record them for confirmation later
    content = blob[1].data_stream.read()
    fingerprints = tokenizer.fingerprint(content)
    fingerprints = tokenizer.getFingerprints(fingerprints)
    # temporary solution, need to map blob_id each fingerprint to insert effectively
    sim_detect.populate(db,cursor,table,blob[0],blob[1],fingerprints)
    #record.append((blob.hexsha,fingerprints))

# Filter operation from blobs
#if len(record) > 0:
#  print "-- Insert Record --"
#  print record
#  print "\n"
#print "New blob set:"
#for blob in blobs:
#  print blob
#print "# of blobs:",len(blobs)
#print "\n"

# Duplicate check
dbAdapter.duplicate(cursor,table)

print "Disconnecting from database"
# Close connection
dbAdapter.disconnect(db)
