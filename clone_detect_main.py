import os
import sys
import inspect
from argparse import ArgumentParser, ArgumentTypeError
from decimal import *
path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.append(path + '/git_proc/')
sys.path.append(path + '/tokenizer/')
sys.path.append(path + '/database/')
sys.path.append(path + '/sim_detect/')
sys.path.append(path + '/report_gen/')
from database import dbAdapter
from sim_detect import sim_detect
from git_proc import git_proc
from tokenizer import tokenizer
from report_gen import report_gen

sys.dont_write_bytecode = True

if __name__ == "__main__":

  if len(sys.argv) < 4:
    raise ArgumentTypeError('Too few arguments')
  else:
    parser = ArgumentParser(prog="Clone Finder", description="Finding clone in repository using a hash MySQL database.")
    parser.add_argument('-r', action="store",dest="repository",help='Path of git repository', required=False, type=str)
    parser.add_argument('-host', action="store", dest="host", help='Host of mysql database', required=True, type=str)
    parser.add_argument('-u', action="store", dest="user", help='MySQL user', required=True, type=str)
    parser.add_argument('-p', action="store", dest="pwd", help="MySQL pass", required=True, type=str)
    parser.add_argument('-d', action="store", dest="database", help="MySQL database", required=True, type=str)
    parser.add_argument('-t', action="store", dest="db_table", help="MySQL table",required=False, type=str)
    parser.add_argument('-s', action="store", dest="similarity", help="Similarity is by default 0.5", required=False, type=Decimal)
    parser.add_argument('-f', action="store", dest="filename", help="Summary report name", required=False, type=str)
    parser.add_argument('--version', action="version",version="%(prog)s 0.9a")
    arguments = parser.parse_args()

    repo = git_proc.access_gitDB(arguments.repository)
    if repo == None:
      print "Not a repo"
    commits = git_proc.getCommits(repo)
    trees = git_proc.getTrees(repo,commits)
    blobs = git_proc.getBlobs(trees)

    print "Connecting to database"
    db_info = dbAdapter.connect(database=arguments.database,host=arguments.host,user=arguments.user,passwd=arguments.pwd)
    db = db_info[0]
    cursor = db_info[1]

    dbAdapter.createTable(cursor, arguments.db_table)

    print "Reading repository",arguments.repository,"..."
    if arguments.similarity is not None:
      report = sim_detect.queryDB(db,cursor,arguments.db_table,blobs,exist=True)
    else:
      report = sim_detect.queryDB(db,cursor,arguments.db_table,blobs,exist=True,sim=arguments.similarity)
    print "Done reading repository", arguments.repository,"."

    print "Generating summary..."
    table = report_gen.genTable(report)

    if arguments.filename is not None:
      report_gen.createHTMLReport(table,filename=arguments.filename)
    else:
      report_gen.createHTMLReport(table)
    print "Done generating summary."

    print "Disconnecting from database"
    dbAdapter.disconnect(db)
