import MySQLdb
import time
from decimal import *
import sys
sys.path.append('/Users/minh-long/git/clone_detection/tokenizer/')
sys.path.append('/Users/minh-long/git/clone_detection/database/')
sys.path.append('/Users/minh-long/git/clone_detection/git_proc/')
sys.path.append('/Users/minh-long/git/clone_detection/report_gen/')

import tokenizer
import dbAdapter
import git_proc
import report_gen

# Decimal precision
getcontext().prec = 4

# Sort out result of query of a particular blob's fingerprint
def sortOutQueryResult(cursor,table,blob,fp_tuple):
  result = dict()
  src_blob_id = blob.hexsha
  for fp in fp_tuple:
    fields = dbAdapter.selectHash(cursor,table,blob.hexsha,fp)
    for field in fields:
      commit_id = field[0]
      #print "Commit id=",commit_id
      blob_path = field[1]
      #print "Blob path=",blob_path
      blob_id = field[2]
      #print "Blob id=",blob_id
      if blob_id in result.keys():
        result[blob_id].append((commit_id,blob_path))
      else:
        result[blob_id] = [(commit_id,blob_path)]
      #print "\n"
  return (blob,result,len(fp_tuple))

# Calculate similarity
# returns src commit blob query hits and similarity
def simCal(sortedQuery):
  blob = sortedQuery[0]
  cal_simHits = dict()
  total_hashes = sortedQuery[2]
  keys = sortedQuery[1].keys()
  for key in keys:
    values = sortedQuery[1][key]
    similarity = Decimal(len(values))/Decimal(total_hashes)
    # construct dictionary
    cal_simHits[key] = [values,similarity]
  return(blob,cal_simHits)

# Query database to establish similarity report
# exist parameter -> Take existing blob ids in database into account during the similarity report generation
# Default value = True
# sim -> similarity threshold
# Default value = 0.5 -> 50%
def queryDB(db,cursor,table,blobs,exist=True,sim=0.5):
  # Stores each blob's id finding in the list
  result = []
  cal_result = []
  for b in blobs:
    commit = b[0]
    blob = b[1]
    content = blob.data_stream.read()
    query = dbAdapter.filterHash(cursor,table,blob.hexsha)

    # generate fingerprints into sets
    fp_set = tokenizer.tokenizer.fingerprint(content)
    # extract fingerprints from set into tuple
    fp_tuple = tokenizer.tokenizer.getFingerprints(fp_set)
    # Query in database
    sortedQuery = sortOutQueryResult(cursor,table,commit,blob,fp_tuple)
    result.append(sortedQuery)
    # Dont populate if it already exists
    if query is None:
      # Database to be populated
      populate(db,cursor,table,commit,blob,fp_tuple)
  for r in result:
    cal_result.append(simCal(r))
  # filter out similarity under a certain threshold
  for cal in cal_result:
    if cal[3] < sim:
      try:
        cal_result.remove(cal)
      except ValueError:
        pass
      except:
        raise
  return cal_result

# Insert into database
def populate(db,cursor,table,commit_id,blob_id,blob_hashes):
  for h in blob_hashes:
    dbAdapter.insert(db,cursor,table,commit_id,blob_id,h)

# Generate similarity report
def genReport(blob_id,blob_hash,summary_data,sim):
  report = []
  ignored = []
  keys = summary_data.keys()

  for key in keys:
    query_hits = summary_data[key]
    similar = Decimal(len(query_hits))/Decimal(len(blob_hash))
    values = summary_data.get(key)
    commitID = values[0][0]
    path = values[0][1]
    blobID = values[0][2]
    if similar < Decimal(sim):
      ignored.append((commitID,path,blobID,similar))
    else:
      report.append((commitID,path,blobID,similar))
  return (report,ignored)

def find_duplicates(cursor,table,similar_rating_obj):
  fingerprints = similar_rating_obj.getFingerprints()
  blob_id = similar_rating_obj.getBlob().hexsha
  for fingerprint in fingerprints:
    '''Query database for previously known record'''
    query = dbAdapter.selectHash(cursor,table,blob_id,fingerprint)
    similar_rating_obj.addHits(query)
