import sys
import hashlib
import sha3
# default hash function sha3 512 bit
def hashing(string, hash_function='sha3_224'):
  s = hashlib.new(hash_function)
  s.update(string)
  return s.hexdigest()
