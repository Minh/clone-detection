from winnowing import *
from decimal import *
import code_segment_samples
import hashing

# fingerprinting of code, using sha1 -> possible to use other
def fingerprint(code_seg):
  def hash_sha3_224(text):
    import hashlib
    hs = hashlib.new('sha3_224')
    hs.update(text)
    hs = hs.hexdigest()
    hs = int(hs, 16)
    return hs
  winnow.hash_function = hash_sha3_224
  seg_fingerprint = winnow(code_seg,k=5)
  return seg_fingerprint

# Get only fingerprint data from winnow output
def getFingerprints(fingerprints):
  return zip(*fingerprints)[1]

# Check fingerprint code_seg2->code_seg1
def similarity(code_seg1, code_seg2):

  fp1 = fingerprint(code_seg1)
  fp1_hash = zip(*fp1)
  #print "--- FP1 hash ---"
  #print fp1_hash[1]
  fp2 = fingerprint(code_seg2)
  fp2_hash = zip(*fp2)
  #print "--- FP2 hash ---"
  #print fp2_hash[1]
  common = []
  if len(fp1_hash[1]) > len(fp2_hash[1]):
    for x in fp2_hash[1]:
      if x in fp1_hash[1]:
        common.append(x)
  elif len(fp1_hash[1]) < len(fp2_hash[1]):
    for x in fp1_hash[1]:
      if x in fp2_hash[1]:
        common.append(x)
  else:
    for x in fp2_hash[1]:
      if x in fp1_hash[1]:
        common.append(x)
  #print "--- Common ---"
  #print common

  # define decimal precision
  getcontext().prec=4
  #print "Length hash1:",len(fp1_hash[1])
  #print "Length hash2:",len(fp2_hash[1])
  #print "Length common:",len(common)

  fp1_per = Decimal(len(common))/Decimal(len(fp1_hash[1]))
  fp2_per = Decimal(len(common))/Decimal(len(fp2_hash[1]))

  # fp2_per shows the percentage how similar it is relatively to the code_seg1
  # fp1_per shows the percentage how similar it is relatively to the code_seg2
  return (fp1_per,fp2_per)

if __name__ == "__main__":
  print "--- Fingerprinting code segment ---"
  similar = similarity(code_segment_samples.code_sample1.replace(' ','').replace('\n',''),code_segment_samples.code_sample1.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample1.replace(' ','').replace('\n',''),code_segment_samples.code_sample2.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample1.replace(' ','').replace('\n',''),code_segment_samples.code_sample3.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample1.replace(' ','').replace('\n',''),code_segment_samples.code_sample4.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample1.replace(' ','').replace('\n',''),code_segment_samples.code_sample5.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample5.replace(' ','').replace('\n',''),code_segment_samples.code_sample6.replace(' ','').replace('\n',''))
  print similar
  print "\n"
  similar = similarity(code_segment_samples.code_sample5.replace(' ','').replace('\n',''),code_segment_samples.code_sample7.replace(' ','').replace('\n',''))
  print similar
  print "\n"
