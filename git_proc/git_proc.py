from git import Blob, Head, Repo, Tree, Commit, Git, GitDB
import time
import datetime
import os, os.path
import sys

# odbt = GitDB - consumes less memory when handling huge files
# but will be 2=5 times slower when extracting a large quantities small of objects from a densely packed repo

# odbt = GitCmdObjectDB - operates very fast under all conditions, consumes additional memory for the process itself
# when extracting large files - memory usage will be much higher compared to GitDB

def access_gitDB(path):
  try:
    repo = Repo(str(path), odbt=GitDB)
    #repo = Repo(str(path), odbt=GitCmdObjectDB)
  except:
    raise
  return repo

def epochToDate(epoch):
  return datetime.datetime.fromtimestamp(epoch).strftime('%Y-%m-%d %H:%M:%S')

def blob_information(repo):
  hc = repo.head.commit
  htc = hc.tree
  hc != htc
  blobs = htc.blobs
  print "-- Blobs --"

  print "# of blobs:", len(blobs)
  print "Blob path:", htc.path
  for x in range(0,len(blobs)):
    print "Blob", x
    print "Blob name:", htc.blobs[x].name
    print "Blob mode:", htc.blobs[x].mode
    print "Blob size:", htc.blobs[x].size
    print "Blob content:", htc.blobs[x].data_stream.read(), "\n"

# get heads of git repo
def getHeads(repo):
  heads = repo.heads
  return heads

# return list of commit ids
def getCommits(repo):
  heads = getHeads(repo)
  commit_ids = []
  for head in heads:
    commits = repo.iter_commits(head, None, all=True)
    for commit in commits:
      commit_ids.append(commit)
      #print commit.hexsha, " - ", commit.message
  commit_ids = dict.fromkeys(commit_ids).keys()
  #print "# of commits:", len(commit_ids)
  #for commit_id in commit_ids:
    #print commit_id
  return commit_ids

# return list of trees
def getTrees(repo,commits):
  commit_trees = []
  for commit in commits:
    trees = repo.iter_trees(commit,None,all=True)
    for tree in trees:
      commit_trees.append((commit,tree))
  commit_trees = dict.fromkeys(commit_trees).keys()
  #print "# of trees:", len(commit_trees)
  #for tree in commit_trees:
    #print tree
  return commit_trees

# return list of blobs
def getBlobs(trees):
  blobs = []
  for tree in trees:
    objects = tree[1].traverse(visit_once=True)
    if objects == None:
      return None
    else:
      for obj in objects:
        if obj.mode == Blob.file_mode:
          commitID = tree[0]
          #print  commitID,"->",obj.path
          blobs.append((commitID.hexsha,obj))
  return blobs
