import os
import sys
import inspect
path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.append(path + '/git_proc/')
sys.path.append(path + '/tokenizer/')
sys.path.append(path + '/database/')
sys.path.append(path + '/sim_detect/')
from git_proc import git_proc
from tokenizer import hashing
from tokenizer import tokenizer
from tokenizer import code_segment_samples
from database import dbAdapter
from sim_detect import sim_detect
from decimal import *

# table to be used for this test
table = "test_hashs"

# Access to database
db_info = dbAdapter.connect("anagram_hash")
db = db_info[0]
cursor = db_info[1]
dbAdapter.createTable(cursor, table)


# Code segment sample
file_sample_uuid = "924175ec899044d4b8620220d112894b"
code_segment_sample = code_segment_samples.code_segment1

file_duplicate_uuid = "924175ec899044d4b8620220d112894e"
code_segment_duplicate = code_segment_samples.code_segment1

file_include_uuid = "924175ec899044d4b8620220d112794b"
code_segment_include = code_segment_samples.code_segment2

file_different_uuid = " 922175ec899044d4b8620220d112894b"
code_segment_different = code_segment_samples.code_segment5

file_sample_include_5_uuid = "924175ec899044d4b8620220d012894b"
code_segment_include_5 = code_segment_samples.code_segment6

file_sample_include_6_uuid = "924175ec899044d4b8620220d012894c"
code_segment_include_6_uuid = code_segment_samples.code_segment7

# Add add code segment sample into hash database
# winnowing method
sample_fingerprint = tokenizer.fingerprint(code_segment_sample)
sample_fingerprint = tokenizer.getFingerprints(sample_fingerprint)

print "-- Sample fingerprint --"
sim_detect.populate(db,cursor,table,file_sample_uuid,sample_fingerprint)

#for fingerprint in sample_fingerprint:
  # Insert into database, no ngram available
  #print fingerprint
#print sample_fingerprint
#print "Length:",len(dict.fromkeys(sample_fingerprint).keys())
#print "\n"
  #dbAdapter.insert(db,cursor,table,file_sample_uuid,fingerprint)

sample_duplicate = tokenizer.fingerprint(code_segment_sample)
sample_duplicate = tokenizer.getFingerprints(sample_duplicate)
#print "-- Duplicate fingerprint --"
#print sample_duplicate
#print "\n"
#print list(sample_duplicate)

# Query ids from table
#fields = dbAdapter.hashQuery(cursor,table,sample_duplicate)
print "-- Sample duplicates --"
print sample_duplicate,"\n"
#print [repr(x) for x in sample_duplicate]
#print "# of fingerprints:",len(sample_duplicate)

#print "-- Query request result --"
#print fields
#print "\n"

print "-- Similarity Report --"
report = sim_detect.simDB(cursor,table,sample_duplicate,sim=0.5)
print "--- Ignored ---"
print report[1]
print "--- Report ---"
print report[0]
print "\n"

print "Disconnecting from database"
dbAdapter.disconnect(db)
